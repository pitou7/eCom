<?php
    require 'admin/database.php';

    if(!empty($_GET['id']))
    {
        $id = checkInput($_GET['id']);
    }

    //Recuperation d'un article
    $db = Database::connect();
    $statement = $db->prepare('SELECT item.id, item.name, item.description, item.price, item.image, categories.name as category FROM item LEFT JOIN categories ON item.category = categories.id WHERE item.id = ?');
    $statement->execute(array($id));
    $item = $statement->fetch();        //Tout est stocké ici
    $prixT = $item['price'] * 650 ;
    Database::disconnect();

    function checkInput($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>EVAT GUANITO</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <link href='http://fonts.googleapis.com/css?family=Holtwood+One+SC' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="../css/style.css">
    </head>
    <body>
        <h1 class="text-logo"><span class="glyphicon glyphicon-globe"></span> EVAT GUANITO <span class="glyphicon glyphicon-globe"></span></h1>
        <div class="container commande">
            <div>
                <label>Vous avez sélectionné : <?= $item['name']; ?> dont le prix est : <?= number_format($item['price'], 2, '.', '') .' €'; ?><br>
                Si vous continuez cet achat, votre facture s'élèvera à : <?php echo $prixT; ?> Fcfa</label> 
            </div>
            <div class="form-actions lien2">
                <p>Souhaitez-vous finaliser cet achat ??</p>
                <a class="btn btn-success" href="facture.php">Oui</a>
                <a class="btn btn-warning" href="index.php">Annuler</a>
            </div>
        </div>
    </body>
</html>