<?php
    require 'database.php';
    /**
     *Si le $_GET['id'] qui est passé à l'URL n'est pas vide, alors on sauvegarde sa valeur dans la variable $id. Et on le nettoie avec la fonction checkInput()
     */
    if(!empty($_GET['id'])) 
    {
        $id = checkInput($_GET['id']);
    }

    $nameError = $descriptionError = $priceError = $imageError = $categoryError = $name = $description = $price = $image = $category = "";

    if(!empty($_POST))
    {
        $name               = checkInput($_POST['name']);
        $description        = checkInput($_POST['description']);
        $price              = checkInput($_POST['price']);
        $category           = checkInput($_POST['category']);
        $image              = checkInput($_FILES["image"]["name"]);
        $imagePath          = '../images/' . basename($image);
        $imageExtension     = pathinfo($imagePath, PATHINFO_EXTENSION);
        $isSuccess          = true;
        $isUploadSuccess    = false;

        if(empty($name))
        {
            $nameError = "Vous devez remplir ce champ !!";
            $isSuccess = false;
        }
        if(empty($description))
        {
            $descriptionError = "Vous devez remplir ce champ !!";
            $isSuccess = false;
        }
        if(empty($price))
        {
            $priceError = "Vous devez remplir ce champ !!";
            $isSuccess = false;
        }
        if(empty($category))
        {
            $categoryError = "Vous devez remplir ce champ !!";
            $isSuccess = false;
        }
        if(empty($image))       // le input file est vide, ce qui signifie que l'image n'a pas ete update
        {
            $isImageUpdated = false;
        } 
        else
        {
            $isImageUpdated = true;
            $isUploadSuccess = true;
            if($imageExtension != "png" && $imageExtension != "jpeg" && $imageExtension != "jpg" && $imageExtension != "gif")
            {
                $imageError = "Les extensions des fichiers autorisés sont : .png, .jpeg, .jpg et .gif";
                $isUploadSuccess = false;
            }
            if(file_exists($imagePath))
            {
                $imageError = "Ce fichier existe déjà !!";
                $isUploadSuccess = false;
            }
            if($_FILES["image"]["size"] > 350000)
            {
                $imageError = "Votre fichier ne doit pas dépaasé 350KB";
                $isUploadSuccess = false;
            }
            if($isUploadSuccess)
            {
                if(!move_uploaded_file($_FILES["image"]["tmp_name"], $imagePath))
                {
                    $imageError = "Une erreur s'est produite lors de l'importation de votre fichier";
                    $isUploadSuccess = false;
                }
            }
        }
        if (($isSuccess && $isImageUpdated && $isUploadSuccess) || ($isSuccess && !$isImageUpdated)) 
        { 
            $db = Database::connect();
            if($isImageUpdated)
            {
                $statement = $db->prepare("UPDATE item  set name = ?, description = ?, price = ?, category = ?, image = ? WHERE id = ?");
                $statement->execute(array($name,$description,$price,$category,$image,$id));
            }
            else
            {
                $statement = $db->prepare("UPDATE item  set name = ?, description = ?, price = ?, category = ? WHERE id = ?");
                $statement->execute(array($name,$description,$price,$category,$id));
            }
            Database::disconnect();
            header("Location: index.php");
        }
        else if($isImageUpdated && !$isUploadSuccess)
        {
            $db = Database::connect();
            $statement = $db->prepare("SELECT * FROM item where id = ?");
            $statement->execute(array($id));
            $item = $statement->fetch();
            $image          = $item['image'];
            Database::disconnect();
        }
    }
    else
    {
        $db = Database::connect();
        $statement = $db->prepare("SELECT * FROM item where id = ?");
        $statement->execute(array($id));
        $item = $statement->fetch();
        $name           = $item['name'];
        $description    = $item['description'];
        $price          = $item['price'];
        $category       = $item['category'];
        $image          = $item['image'];
        Database::disconnect();
    }


    function checkInput($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
        <title>EVAT GUANITO</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <link href='http://fonts.googleapis.com/css?family=Holtwood+One+SC' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="../css/style.css">
    </head>
    <body>
        <h1 class="text-logo"><span class="glyphicon glyphicon-globe"></span> EVAT GUANITO <span class="glyphicon glyphicon-globe"></span></h1>
        <div class="container admin">
            <div class="row">
                <div class="col-sm-6">
                    <h1><strong>Modifier un article  </strong></h1>
                    <br>
                    <form class="form" action="<?php echo 'update.php?id='.$id;?>" role="form" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="name">Nom : </label>
                            <input type="text" name="name" placeholder="Nom" class="form-control" id="name" value="<?php echo $name ?>">
                            <span class="help-inline"><?= $nameError ?></span>
                        </div>
                        <div class="form-group">
                            <label for="description">Description : </label>
                            <input type="text" id="description" name="description" placeholder="Description" class="form-control" value="<?= $description ?>">
                            <span class="help-inline"><?= $descriptionError ?></span>
                        </div>
                        <div class="form-group">
                            <label for="price">Prix (en €) : </label>
                            <input type="number" step="1" name="price" id="price" placeholder="Prix" class="form-control" value="<?= $price ?>">
                            <span class="help-inline"><?= $priceError ?></span>
                        </div>
                        <div class="form-group">
                            <label for="category">Catégorie : </label>
                            <select name="category" id="category"  class="form-control">
                                <?php
                                    $db = Database::connect();
                                    foreach ($db->query('SELECT * FROM categories') as $row) 
                                    {
                                        if($row['id'] == $category)
                                            echo '<option selected="selected" value="'. $row['id'] .'">'. $row['name'] . '</option>';
                                        else
                                            echo '<option value="'. $row['id'] .'">'. $row['name'] . '</option>';;
                                    }
                                    Database::disconnect();
                                ?>
                            </select>
                            <span class="help-inline"><?= $categoryError ?></span>
                        </div>
                        <div class="form-group">
                            <label for="image">Image:</label>
                            <p><?php echo $image;?></p>
                            <label for="image">Sélectionner une nouvelle image:</label>
                            <input type="file" id="image" name="image">
                            <span class="help-inline"><?= $imageError ?></span>
                        </div>
                        <br>
                        <div class="form-actions">
                            <a href="index.php" class="btn btn-primary" ><span class="glyphicon glyphicon-circle-arrow-left"></span>Retour</a>
                            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-pencil"></span>Modifier</button>
                        </div>
                    </form> 
                </div>
                <div class="col-sm-6 site">
                    <div class="thumbnail">
                        <img src="<?php echo '../images/'.$image;?>" alt="...">
                        <div class="price"><?php echo number_format((float)$price, 2, '.', ''). ' €';?></div>
                          <div class="caption">
                            <h4><?php echo $name;?></h4>
                            <p><?php echo $description;?></p>
                          </div>
                    </div>
                </div>          
            </div>
        </div>
    </body>
</html>