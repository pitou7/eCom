<?php
    require 'database.php';

    if(!empty($_GET['id']))
    {
        $id = checkInput($_GET['id']);
    }

    //Recuperation d'un article
    $db = Database::connect();
    $statement = $db->prepare('SELECT item.id, item.name, item.description, item.price, item.image, categories.name as category FROM item LEFT JOIN categories ON item.category = categories.id WHERE item.id = ?');
    $statement->execute(array($id));
    $item = $statement->fetch();        //Tout est stocké ici
    Database::disconnect();

    function checkInput($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
        <title>EVAT GUANITO</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <link href='http://fonts.googleapis.com/css?family=Holtwood+One+SC' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="../css/style.css">
    </head>
    <body>
        <h1 class="text-logo"><span class="glyphicon glyphicon-globe"></span> EVAT GUANITO <span class="glyphicon glyphicon-globe"></span></h1>
        <div class="container admin">
            <div class="row">
                <div class="col-sm-6">
                    <h1><strong>Voir un article  </strong></h1>
                    <br>
                    <form>
                        <div class="form-group">
                            <label>Nom : </label> <?= $item['name']; ?>
                        </div>
                        <div class="form-group">
                            <label>Description : </label> <?= $item['description']; ?>
                        </div>
                        <div class="form-group">
                            <label>Prix : </label> <?= number_format($item['price'], 2, '.', '') .' €'; ?>
                        </div>
                        <div class="form-group">
                            <label>Categorie : </label> <?= $item['category']; ?>
                        </div>
                        <div class="form-group">
                            <label>Image : </label> <?= $item['image']; ?>
                        </div>
                    </form>
                    <br>
                    <div class="form-actions">
                        <a href="index.php" class="btn btn-primary" ><span class="glyphicon glyphicon-circle-arrow-left"></span>Retour</a>
                    </div>
                </div> 
                <div class="col-sm-6 site">   
                    <div class="thumbnail">
                        <img src="<?php echo '../images/' . $item['image'] ; ?>" alt="...">
                        <div class="price"><?=  number_format($item['price'], 2, '.', '') .' €'; ?></div>
                        <div class="caption">
                            <h4><?= $item['name']; ?></h4>
                            <p><?= $item['description']; ?></p>
                        </div>
                    </div>
                </div>             
            </div>
        </div>
    </body>
</html>