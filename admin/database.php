<?php
class Database 
{

    private static $connection = null;

    public static function connect()
    {
        try 
        {//Recuperation des infos de la BD
            self::$connection =  new PDO('mysql:host=localhost;dbname=eva_guanito','root','root', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
        } catch (PDOException $e) 
        {
            die($e->getMessage());
        }
        return self::$connection;
    }

    public static function disconnect()
    {
        self::$connection = null;
    }
}


?>